package hw;
/*
Task 9
If we list all natural numbers less than 10, multiples of 3 or 5, we get 3, 5, 6 and 9.
The sum of these multiples is 23.
Find the sum of all numbers that are multiples of 3 or 5, starting with 0
and up to 1,000.
 */

import static java.lang.System.out;

public class Task9 {
    public static void main(String[] args) {
        int sum = 0;
        for (int j = 0; j <= 1000; j++) {
            if (j % 3 == 0 & j % 5 == 0) {
                sum = sum + j;
            }
        }
        out.println(sum);
    }
}
