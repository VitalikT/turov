package hw;
/*
Task 6
Print all eight-digit numbers to the console, digits in
which do not repeat. These numbers should be divisible by
12345, no residue. Show total found
numbers
 */

public class Task6 {
    public static void main(String[] args) {
        int i = 10000000;
        int sum = 0;
        while (i<100000000){
            i++;
            if (i%12345!=0)
                continue;
            sum++;
        }
        System.out.print(sum);
    }
}
