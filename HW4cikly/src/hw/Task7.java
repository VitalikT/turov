package hw;
/*
Task 7
Show bit representation of variable value
type int, using only one loop, control variable, console output and bit operations.
Do not use strings or any other ready-made functions (methods).
 */

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the number: ");
        int value = scanner.nextInt();

        scanner.close();
        if (value > 0) {
            for (int step = 31; step >= 0; step--) {
                if ((value / (Math.pow(2, step))) >= 1) {
                    System.out.print(1);
                    value -= Math.pow(2, step);
                } else
                    System.out.print(0);
            }
        }
        else if (value < 0 ){
            value = ~value +1;
            System.out.println(value);
            for (int step = 31; step >= 0; step--) {
                if ((value / (Math.pow(2, step))) >= 1) {
                    System.out.print(0);
                    value -= Math.pow(2, step);
                } else
                    System.out.print(1);
            }
        }
    }
}
