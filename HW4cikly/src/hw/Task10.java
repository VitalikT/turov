package hw;
/*
Task 10
2520 is the smallest number that can be divided into
each of the numbers from 1 to 10, without a remainder.
Write a program that calculates the smallest positive number that is divided into
all numbers from 1 to 20.
 */

public class Task10 {
    public static void main(String[] args) {
        for (int i = 1; i < 400000000; i++) {
            int sum = 0;
            for (int j = 1; j <= 20; j++) {
                if (i%j==0){
                    sum++;
                }
            }
            if (sum==20) {
                System.out.print(i+" ");

            }
        }
    }
}
