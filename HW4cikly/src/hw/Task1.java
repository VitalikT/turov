package hw;

/*
Exercise 1
Fibonacci numbers are a sequence of numbers in which the first two numbers are 0 and 1,
and each subsequent number is equal to the sum of the two previous ones.
Fibonacci range from
0 to 10,000,000
 */


import static java.lang.System.out;

public class Task1 {
    public static void main(String[] args) {
        int i = 0;
        int y = 1;
        int f = 10000000;
        int sum = i + y;
        while (sum <= f) {
            out.print(" " + i + " " + y + " " + sum);
            i = sum + y;
            y = i + sum;
            sum = i + y;
        }
    }
}
