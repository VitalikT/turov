package iskluchenia.task4;


import iskluchenia.task2.MyException;

import java.util.Random;

public class Task4 {

    Random in = new Random();

    int first, last, middle, search, array[];

    public void binarySearchMethod()  {

        array = new int[in.nextInt(10)];
        for (Integer c : array)
            array[c] = in.nextInt(10);
        search = in.nextInt(10);
        first = 0;
        last = array.length - 1;
        middle = (first + last) / 2;
        try {
            if (arraySortingCheck(array)) {
                throw new MyException();
            }
            while (first <= last) {
                if (array[middle] < search) {
                    first = middle + 1;
                } else if (array[middle] == search) {
                    System.out.println(search + " found at location " + (middle + 1) + ".");
                    break;
                } else {
                    last = middle - 1;
                    middle = (first + last) / 2;
                }
            }
            if (first > last) {
                System.out.println(search + " isn't present in the list.\n");
            }
        }catch (MyException m){
            System.out.println("The array is not sorted");
        }
    }

    private boolean arraySortingCheck(int[] checkArray) {
        boolean sorted = false;
        for (int i = 1; i < checkArray.length; i++) {
            if (checkArray[i - 1] > checkArray[i]) {
                sorted = true;
                break;
            }
        }
        return sorted;
    }
}

