package iskluchenia.task3;

/*
Task 3
Create a class whose objects will be immutable.
A class encapsulates information about a triangle on
plane (the length of each of its edges). The lengths of the sides are set in the constructor.
If on the set sides it is impossible
to build a triangle, the designer should rush
an exception.
Test the class using JUnit tests.
*/

public class TriangleConstructor {

    private int sideOne;
    private int sideTwo;
    private int sideTree;

    public TriangleConstructor(int sideOne, int sideTwo, int sideTree) {

        this.sideOne = sideOne;
        if (this.sideOne > this.sideTwo + this.sideTree){
            throw new  RuntimeException("\n" +
                    "The sum of two parties cannot be less than the value of a third party");
        }
        this.sideTwo = sideTwo;
        if (this.sideTwo > this.sideOne + this.sideTree){
            throw new  RuntimeException("\n" +
                    "The sum of two parties cannot be less than the value of a third party");
        }
        this.sideTree = sideTree;
        if (this.sideTree > this.sideTwo + this.sideOne){
            throw new  RuntimeException("\n" +
                    "The sum of two parties cannot be less than the value of a third party");
        }
    }
}

