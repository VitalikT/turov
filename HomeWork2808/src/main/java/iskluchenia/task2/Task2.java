package iskluchenia.task2;
/*Task2
 * Create a method that takes a number as input. When,
 * if the number is negative, the method should be thrown
 * checked exception. If the number is greater than 100, must
 * an unchecked exception is thrown. Create your exceptions for this example.
 * Test the method using JUnit tests.
 */


public class Task2 {

    public void checkingForExceptions(int number) {
        try {
            if (number < 0) {
                throw new MyException();
            }
            if (number > 100) {
                throw new MyRuntimeException();
            }
        } catch (MyException e) {
            System.out.println("Number is less than zero");
        } catch (MyRuntimeException e) {
            System.out.println("Number is more than a hundred");
        }
    }
}