package iskluchenia.task1;

/*
Exercise 1
Create a method that outputs to the console the result of integer division of the number entered from the keyboard by
the values of the elements of a one-dimensional array of integers,
filled randomly - from -10 to 10. The length of the array is random - from 1 to 10.
Handle all possible exceptions in
this method.
*/

import java.util.Random;

public class Task1 {
    KeyingAnInteger keyingAnInteger = new KeyingAnInteger();
    Random random = new Random();

    public void resultOfIntegerDivisionOfNumber() {
        int[] randomArray = randomArrayOfIntegersFilledRandomly();
        int randomNumber = keyingAnInteger.keyingAnInteger();
        try {
            randomArray[4] = 5;
            for (int i = 0; i < randomArray.length; i++) {
                int arrayNumber = randomArray[i];
                int resultDivision = randomNumber / arrayNumber;
                System.out.println(resultDivision);
            }
        } catch (ArithmeticException e) {
            System.err.println("Division by zero");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("The cell number of the array is specified beyond its length");
        } finally {
            System.out.println("Thanks to everybody, you're free.");
        }
    }

    public int[] randomArrayOfIntegersFilledRandomly() {

        int[] array = new int[random.nextInt(10)];
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(20) - 10;
        }
        return array;
    }
}
