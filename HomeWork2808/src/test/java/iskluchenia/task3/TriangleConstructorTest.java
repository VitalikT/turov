package iskluchenia.task3;

import iskluchenia.task2.MyRuntimeException;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

class TriangleConstructorTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @org.junit.jupiter.api.Test
    void TriangleConstructorTest() {
        exception.expect(MyRuntimeException.class);
        exception.expectMessage("The sum of two parties cannot be less than the value of a third party");
    }

}