package iskluchenia.task2;

import org.junit.Rule;
import org.junit.rules.ExpectedException;
class Task2Test {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @org.junit.jupiter.api.Test
    void checkingForExceptions() {
        exception.expect(MyException.class);
        exception.expectMessage("Number is less than zero");
        exception.expect(MyRuntimeException.class);
        exception.expectMessage("Number is more than a hundred");
    }
}