package genericsTask1;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class MyArrayListTest {

    @Test
    void getSize() {
        MyArrayList test = new MyArrayList();
        Assert.assertNotNull(test.getSize());
    }

    @Test
    void testToString() {
        MyArrayList test = new MyArrayList();
        Assert.assertTrue(test.toString(),true);
    }

    @Test
    void pushBack() {
        MyArrayList test = new MyArrayList();
        Integer argument = 3;
        boolean extResult = true;
        boolean result = test.pushBack(argument );
        Assert.assertEquals(result,extResult);
    }

    @Test
    void popFront() {
        MyArrayList test = new MyArrayList();
        Integer argument = 3;
        boolean extResult = true;
        boolean result = test.pushBack(argument );
        Assert.assertEquals(result,extResult);
    }

    @Test
    void pushFront() {
        MyArrayList test = new MyArrayList();
        Assert.assertNotNull(test.pushFront(3));
    }

    @Test
    void insert() {
        MyArrayList test = new MyArrayList();
        Assert.assertNotNull(test.pushFront(5));
    }

    @Test
    void removAt() {
        MyArrayList test = new MyArrayList();
        int argument = 5;
        int extResult = 5;
        int result = test.removAt(argument );
        Assert.assertEquals(result,extResult);
    }

    @Test
    void remove() {
        MyArrayList test = new MyArrayList();
        Object argument = 3;
        Object extResult = 3;
        Object result = test.remove(argument );
        Assert.assertEquals(result,extResult);
    }

    @Test
    void removeAll() {
        MyArrayList test = new MyArrayList();
        Object argument = 3;
        Object extResult = 3;
        Object result = test.remove(argument );
        Assert.assertEquals(result,extResult);
    }

    @Test
    void popBack() {
        MyArrayList test = new MyArrayList();
        Assert.assertNotNull(test.popBack());
    }

    @Test
    void clear() {
        MyArrayList test = new MyArrayList();
        Assert.assertNotNull(test.clear());
    }
}