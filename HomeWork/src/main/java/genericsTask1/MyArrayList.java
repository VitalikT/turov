package genericsTask1;


import java.util.Arrays;

public class MyArrayList<T> {


    /**
     * Shared empty array instance.
     */
    private Object[] data;

    /**
     * Shared empty array instance used for default sized empty instances. We
     * distinguish this from EMPTY_ELEMENTDATA to know how much to inflate when
     * first element is added.
     */
    private static final Object[] DEFAULTCAPACITY_EMPTY_ELEMENTDATA = {};

    /**
     * The size of the MyArrayList
     */
    private int size;

    /**
     * Default capacity.
     */
    private int capacity;

    /**
     * Default initial capacity.
     */
    private final int DEFAULT_CAPACITY = 10;

    /**
     * Construct with a parameter of type int. Initial capacity
     * array.
     */
    public MyArrayList(int capacity) {
        this.capacity = capacity;
        if (capacity > 0) {
            this.data = new Object[capacity];
        } else {
            throw new IllegalArgumentException("Illegal Capacity: " +
                    capacity);
        }
    }

    /**
     * Construct by default (without parameters). Which allocates memory
     * for an array of 10 elements equal to zero .
     */
    public MyArrayList() {
        capacity = DEFAULT_CAPACITY;
        data = new Object[DEFAULT_CAPACITY];
    }

    /**
     * Method getSize for size.
     */
    public int getSize() {
        return size;
    }

    /**
     * Redefine the method and implement the string
     * representation of array elements with a space.
     */
    @Override
    public String toString() {
        return "MyArrayList{" +
                "data=" + Arrays.toString(data).replace(",", " ") +
                '}';
    }

    /**
     * Is a private method! checks enough
     * for storing specified in parameter
     * amount of elements. If the parameter value is less
     * current capacity, then nothing happens. If the value
     * set more than the current capacity, then the array is recreated, the memory is indicated 1.5 times + 1 element more.
     * Existing elements are wrapped into a new array.
     * Existing items should not be lost.
     */
    private void ensureCapacity(int minCapacity) {
        int minExpand = (data != DEFAULTCAPACITY_EMPTY_ELEMENTDATA) ? 0 : DEFAULT_CAPACITY;
        if (minCapacity > minExpand) {
            ensureExplicitCapacity(minCapacity);
        }
    }

    private int calculateCapacity(Object[] elementData, int minCapacity) {
        if (elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
            return Math.max(DEFAULT_CAPACITY, minCapacity);
        }
        return minCapacity;
    }

    private void ensureCapacityInternal(int minCapacity) {
        ensureExplicitCapacity(calculateCapacity(data, minCapacity));
    }

    private void ensureExplicitCapacity(int minCapacity) {
        if (minCapacity - data.length > 0)
            grow(minCapacity);
    }

    /**
     * Increases the capacity to ensure that it can hold at least the
     * number of elements specified by the minimum capacity argument.
     */
    private void grow(int minCapacity) {
        int oldCapacity = data.length;
        int newCapacity = oldCapacity + oldCapacity / 2 + 1;
        while (newCapacity - minCapacity < 0)
            newCapacity = newCapacity + newCapacity / 2 + 1;
        data = Arrays.copyOf(data, newCapacity);
    }

    /**
     * Appends the specified element to the end of this array.
     */
    public boolean pushBack(T e) {
        ensureCapacityInternal(size + data.length - size);
        data[data.length - 1] = e;
        return true;
    }

    /**
     * Removing the first element from an array
     * @return
     */
    public boolean popFront() {
        int index = 0;
        data[index] = null;
        size--;
        for (int i = index + 1; i < data.length - 1; i++) {
            data[i - 1] = data[i];
        }
        return true;
    }

    /**
     * Adding a new element to the beginning of the array.
     */
    public T pushFront(T e) {
        ensureCapacityInternal(size + 1);
        data[size++] = e;
        return e;
    }

    /**
     * Inserts the specified element at the specified position in this
     * array. Shifts the element currently at that position (if any) and
     * any subsequent elements to the right (adds one to their indices).
     */
    public T insert(int index, T element) {
        rangeCheckForInsert(index);
        ensureCapacityInternal(size + 1);
        System.arraycopy(data, index, data, index + 1, size - index);
        data[index] = element;
        size++;
        return element;
    }

    /**
     * Delete one item at a specified index.
     */
    public int removAt(int index) {
        rangeCheckForInsert(index);
        data[index] = null;
        for (int i = index + 1; i < data.length - 1; i++) {
            data[i - 1] = data[i];
        }
        return index;
    }

    /**
     * Delete one item whose value
     * matches the value of the passed parameter
     */
    public T remove(T t) {
        for (int i = 0; i <= data.length - 1; i++){
            if (data[i] == t){
                data[i] = null;
                break;
            }
        }
        return t;
    }

    /**
     * Delete all items whose values
     * matches the value of the passed parameter
     */
    public T removeAll(T t) {
        for (int i = 0; i <= data.length - 1; i++) {
            if (data[i] == t) {
                data[i] = null;
            }
        }
        return t;
    }

    /**
     * Removing the first element from an array
     */
    public boolean popBack() {
        int index = data.length - 1;
        data[index] = null;
        size--;
        return true;
    }

    /**
     * Removes all of the elements from this list.  The list will
     * be empty after this call returns.
     */
    public int clear() {
        for (int i = 0; i <= data.length - 1; i++)
            data[i] = null;
        size = 0;
        return size;
    }

    /**
     * RangeCheck
     */
    private void rangeCheckForInsert(int index) {
        if (index > size || index < 0)
            System.out.println("Index: " + index + ", Size: " + size + ", Capacity: " + capacity);
    }
}
