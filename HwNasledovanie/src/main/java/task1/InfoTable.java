package task1;


public class InfoTable extends Table {
    public static void main(String[] args) {
        InfoTable table = new InfoTable();
        table.table();
        table.showInfo();
        table.table();
                                            //   name         Yes/No          Money Day           Days          Tax
        PersonRate ivanov = new PersonRate("Ivanov", "Yes", 30.0, 21.0, "20.0");
        ivanov.showInfo();                  //   name         Yes/No          Money Hour              Hours                      Tax
        PersonHourly sidorov = new PersonHourly("Sidorov", "No", 3.0, 160.0, "20.0");
        sidorov.showInfo();                          //   name         Yes/No          Money One Work           Number Of Works In Month    Tax
        PersonPiecework petrov = new PersonPiecework("Petrov", "No", 100.0, 6.0, "15.0");
        petrov.showInfo();
        PersonPiecework danilov = new PersonPiecework("Danilov", "Yes", 110.0, 7.0, "15.0");
        danilov.showInfo();
        PersonRate dvarov = new PersonRate("Dvarov", "No", 100.0, 30.0, "Offshore");
        dvarov.showInfo();
        table.table();
    }


}
