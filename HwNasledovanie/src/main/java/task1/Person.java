package task1;

public class Person {

    protected String name;
    private String children;
   // private String typeOfPayment;
    private double moneyDayHourWork;
    private double daysHoursWorksMonth;
    private String tAx;


    public Person(String name, String children, double moneyDayHourWork, double daysHoursWorksMonth, String tAx) {
        this.name = name;
        this.children = children;
        this.moneyDayHourWork = moneyDayHourWork;
        this.daysHoursWorksMonth = daysHoursWorksMonth;
        this.tAx = tAx;
    }

    double children() {
        if (children.equals("Yes")) {
            double ch = 0.0;
            return ch;
        } else {
            double ch = 5.0;
            return ch;
        }
    }

    String tAx() {
        if (children.equals("No") & tAx.equals("Offshore")) {
            return "Offshore";
        }
        if (children.equals("Yes") & tAx.equals("Offshore")) {
            return "Offshore";
        }
        if (children.equals("No")) {
            double tax = Double.parseDouble(tAx);
            tax += 5.0;
            tAx = String.valueOf(tax);
            return tAx;
        } else
            return tAx;
    }

    double piecework() {
        if (daysHoursWorksMonth < 10.0) {
            double sum = moneyDayHourWork * daysHoursWorksMonth;
            return sum;
        } else {
            double sum = moneyDayHourWork * daysHoursWorksMonth * 1.2;
            return sum;
        }
    }

    double hourly() {
        if (daysHoursWorksMonth < 200.0) {
            double sum = moneyDayHourWork * daysHoursWorksMonth;
            return sum;
        } else {
            double sum = moneyDayHourWork * daysHoursWorksMonth * 1.2;
            return sum;
        }
    }

    double rate() {
        if (daysHoursWorksMonth < 25.0) {
            double sum = moneyDayHourWork * daysHoursWorksMonth;
            return sum;
        } else {
            double sum = moneyDayHourWork * daysHoursWorksMonth * 1.2;
            return sum;
        }
    }

    String toPayRate() {
        if (tAx.equals("Offshore")) {
            double sumToPay = rate();
            return String.valueOf(sumToPay);
        } else {
            double tax = Double.parseDouble(tAx);
            double sumToPay = rate() * (1 - (tax + children()) / 100);
            return String.valueOf(sumToPay);
        }
    }

    String toPayHourly() {
        if (tAx.equals("Offshore")) {
            double sumToPay = hourly() / 2;
            double sumToPayTugr = sumToPay / 8;
            String sumToPayS = String.valueOf(sumToPay);
            String sumToPayTugrS = String.valueOf(sumToPayTugr);
            return sumToPayS + "/" + sumToPayTugrS;
        } else {
            double tax = Double.parseDouble(tAx);
            double sumToPay = (hourly() * (1 - (tax + children()) / 100)) / 2;
            double sumToPayTugr = sumToPay / 8;
            String sumToPayS = String.valueOf(sumToPay);
            String sumToPayTugrS = String.valueOf(sumToPayTugr);
            return sumToPayS + "/" + sumToPayTugrS;
        }
    }

    String toPayPiecework() {
        if (tAx.equals("Offshore")) {
            double sumToPay = piecework();
            String sumToPayS = String.valueOf(sumToPay);
            return sumToPayS;
        } else {
            double tax = Double.parseDouble(tAx);
            double sumToPay = piecework() * (1 - (tax + children()) / 100);
            String sumToPayS = String.valueOf(sumToPay);
            return sumToPayS;
        }
    }
}
