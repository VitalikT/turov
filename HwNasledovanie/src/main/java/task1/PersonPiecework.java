package task1;

public class PersonPiecework extends Person {

    private String typeOfPayment = "Piecework";

    public PersonPiecework(String name, String children, double moneyDayHourWork, double daysHoursWorksMonth, String tAx) {
        super(name, children, moneyDayHourWork, daysHoursWorksMonth, tAx);
    }

    public void showInfo() {
        System.out.printf("\n| %-10s| %-25s| %-20s| %-10.2f| %-10s|", name, typeOfPayment, tAx(), piecework(), toPayPiecework());
    }

}
