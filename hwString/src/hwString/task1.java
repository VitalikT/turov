package hwString;
/*
Exercise 1
Enter a string of text from the keyboard, and then one character.
Show indexes on the console and the number of matches (look for
the occurrence of a character in a string). In case the matches are not
found, display the corresponding text.
 */

import java.util.Scanner;

public class task1 {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter text: ");
        String text = sc.nextLine();
        System.out.println("Enter one character: ");
        String character = sc.nextLine();
        int sum = 0;
        for (int index =text.indexOf(character);index <= text.length();index++) {
            index = text.indexOf(character, index);
            if (index==-1) {
                break;
            }
            System.out.println(index + " ");
            sum++;

        }
        System.out.println("Sum:" + sum);

    }
}
