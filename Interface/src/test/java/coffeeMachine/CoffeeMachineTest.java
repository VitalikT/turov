package coffeeMachine;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CoffeeMachineTest {

    @Test
    public void coffeeMachineOn() {
        CoffeeMachine man = new CoffeeMachine();
        String argument = "On";
        boolean extRasult = true;
        boolean result = man.coffeeMachineOn(argument);
        Assert.assertEquals(result, extRasult);
    }

    @Test
    public void coffeeMachineOff() {
        CoffeeMachine man = new CoffeeMachine();
        String argument = "Off";
        boolean extRasult = false;
        boolean result = man.coffeeMachineOff(argument);
        assertEquals(result, extRasult);
    }

    @Test
    public void espressoCoffee() {
        CoffeeMachine man = new CoffeeMachine();
        Assert.assertNotNull(man.espressoCoffee());



    }

    @Test
    public void americanoCoffee() {
        CoffeeMachine man = new CoffeeMachine();
        Assert.assertNotNull(man.americanoCoffee());
    }

    @Test
    public void latCoffee() {
        CoffeeMachine man = new CoffeeMachine();
        Assert.assertNotNull(man.latCoffee(50));
    }

    @Test
    public void cappuccinoCoffee() {
        CoffeeMachine man = new CoffeeMachine();
        Assert.assertNotNull(man.cappuccinoCoffee(50));
    }
}