package coffeeMachine;

public interface Mistake {

    String noCoffee();

    String noWater();

    String wasteCoffeeTankIsFull();

    String noMilk();

}
