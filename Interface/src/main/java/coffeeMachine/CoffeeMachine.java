package coffeeMachine;

public class CoffeeMachine implements ModelCoffeeMachine, Mistake {

    private int coffeeTank = coffeeTank();
    private int wasteCoffeeTank = wasteCoffeeTank();
    private int waterTank = waterTank();
    private int milkTank = milkTank();
    private boolean bOn;

    public CoffeeMachine() {

    }


    boolean coffeeMachineOn(String on) {
        if (on.equals("On")) {
            System.out.println("Coffee machine on");
            return bOn = true;
        } else {
            System.out.println("Turn on the coffee machine. Enter On");
            return bOn = false;
        }

    }


    boolean coffeeMachineOff(String off) {
        if (off.equals("Off"))
            System.out.println("Coffee machine off");
        return bOn = false;
    }


    String espressoCoffee() {
        if (bOn) {
            if (waterTank < 30) {
                return noWater();
            }
            if (coffeeTank < 22) {
                return noCoffee();
            }
            if (wasteCoffeeTank < 22) {
                return wasteCoffeeTankIsFull();
            } else {
                coffeeTank -= 22;
                waterTank -= 30;
                wasteCoffeeTank -= 22;
                return "Your espresso is ready";
            }
        } else {
            return "Turn on the coffee machine";
        }
    }

    String americanoCoffee() {
        if (bOn) {
            if (waterTank < 100 ) {
                return noWater();
            }
            if (coffeeTank < 22) {
                return noCoffee();
            }
            if (wasteCoffeeTank < 22) {
                return wasteCoffeeTankIsFull();
            } else {
                coffeeTank -= 22;
                waterTank -= 30;
                wasteCoffeeTank -= 22;
                return "Your amerikano is ready";
            }
        } else {
            return "Turn on the coffee machine";
        }
    }

    String latCoffee(int milk) {
        if (bOn) {
            if (waterTank < 30) {
                return noWater();
            }
            if (coffeeTank < 22) {
                return noCoffee();
            }
            if (wasteCoffeeTank < 22) {
                return wasteCoffeeTankIsFull();
            }
            if (milkTank < milk){
                return noMilk();
            } else if (milk > 70) {
                return "To much milk";
            } else {
                coffeeTank -= 22;
                waterTank -= 30;
                wasteCoffeeTank -= 22;
                milkTank -= milk;
                return "Your lat is ready";
            }
        } else {
            return "Turn on the coffee machine";
        }
    }

    String cappuccinoCoffee(int milk) {
        if (bOn) {
            if (waterTank < 30) {
                return noWater();
            }
            if (coffeeTank < 22) {
                return noCoffee();
            }
            if (wasteCoffeeTank < 22) {
                return wasteCoffeeTankIsFull();
            }
            if (milkTank < milk){
                return noMilk();
            } else if (milk > 70) {
                return "To much milk";
            } else {
                coffeeTank -= 22;
                waterTank -= 30;
                wasteCoffeeTank -= 22;
                milkTank -= milk;
                return "Your cappuccino is ready";
            }
        } else {
            return "Turn on the coffee machine";
        }
    }

    public String noCoffee() {
        return "Coffee is over";
    }

    public String noWater() {
        return "Water is over";
    }

    public String wasteCoffeeTankIsFull() {
        return "The waste coffee tank is full";
    }

    public String noMilk() {
        return "Missing milk";
    }

    public int coffeeTank() {
        coffeeTank = 1000;
        return coffeeTank;
    }

    public int waterTank() {
        waterTank = 2000;
        return waterTank;
    }

    public int wasteCoffeeTank() {
        wasteCoffeeTank = 1000;
        return wasteCoffeeTank;
    }

    public int milkTank() {
        milkTank = 1000;
        return milkTank;
    }
}
