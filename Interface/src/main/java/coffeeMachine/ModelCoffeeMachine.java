package coffeeMachine;

public interface ModelCoffeeMachine {

    int coffeeTank();

    int waterTank();

    int wasteCoffeeTank();

    int milkTank();


}
