package Task1;
/*
 Exercise 1
 There is a site on which the rating of players is calculated
 various network games.
 The player during registration indicates the nickname, as well as the list
 the games he plays.
 The task is to write a program that:
 registers players in the system (there must be a check,
 is the nickname busy);
 adds a rating to a player in case he wins the game;
 displays a list of games played by all players on the site;
 displays the rating by player name and game;
 displays the top 10 players in a particular game;
 displays the top 10 players, taking into account all the games
*/

import java.util.*;

public class Main {
    public static void main(String[] args) {

        Games game1 = new Games("GTA");
        Games game2 = new Games("RDR");
        Games game3 = new Games("Halo");
        Games game4 = new Games("HalfLife");
        Games game5 = new Games("RE");
        Games game6 = new Games("Batman");
        Games game7 = new Games("Mass");
        Games game8 = new Games("Effect");
        Games game9 = new Games("MGS");
        Games game10 = new Games("Portal");
        Games game11 = new Games("Mario");
        Games game12 = new Games("The Last of Us");

        Player player1 = new Player("Tom");
        player1.setListGames(Arrays.asList(game1, game2, game3, game4, game5, game6, game7, game8));


        Player player2 = new Player("Max");
        player2.setListGames(Arrays.asList(game3, game4, game5, game6, game7, game8, game9, game10, game11, game12));


        Player player3 = new Player("Jack");
        player3.setListGames(Arrays.asList(game1, game2, game3, game4, game8, game9, game10, game11, game12));

        Player player4 = new Player("Lucas");
        player4.setListGames(Arrays.asList(game5, game6, game7, game8, game9, game10, game11, game12));

        Player player5 = new Player("Liam");
        player5.setListGames(Arrays.asList(game3, game4, game5, game6, game7, game8));

        Player player6 = new Player("Luke");
        player6.setListGames(Arrays.asList(game1, game2, game3, game9, game10, game11, game12));

        Player player7 = new Player("Jake");
        player7.setListGames(Arrays.asList(game1, game2, game3, game4, game5, game6, game7));

        Player player8 = new Player("James");
        player8.setListGames(Arrays.asList(game3, game4, game5, game6, game8, game9, game10, game11, game12));

        Player player9 = new Player("Noah");
        player9.setListGames(Arrays.asList(game3, game4, game5, game6, game9, game10, game11, game12));

        Player player10 = new Player("Ryan");
        player10.setListGames(Arrays.asList(game1, game2, game3, game4, game5, game8, game9, game10, game11, game12));

        Player player11 = new Player("Nicolas");
        player11.setListGames(Arrays.asList(game1, game2, game3, game4, game5, game6, game7, game11, game12));

        Player player12 = new Player("Nick");
        player12.setListGames(Arrays.asList(game1, game2, game5, game6, game7, game8, game9, game10, game11, game12));

        game1.setListPlayers(Arrays.asList(player1, player3, player6, player7, player10, player11, player12));
        game2.setListPlayers(Arrays.asList(player1, player3, player6, player7, player10, player11, player12));
        game3.setListPlayers(Arrays.asList(player1, player2, player3, player5, player6, player7, player10, player11, player8));
        game4.setListPlayers(Arrays.asList(player1, player2, player3, player5, player7, player8, player9, player10, player11));
        game5.setListPlayers(Arrays.asList(player1, player2, player4, player5, player7, player8, player9, player10, player11, player12));
        game6.setListPlayers(Arrays.asList(player1, player2, player4, player5, player7, player8, player9, player11, player12));
        game7.setListPlayers(Arrays.asList(player1, player2, player4, player5, player7, player11, player12));
        game8.setListPlayers(Arrays.asList(player1, player2, player3, player4, player5, player8, player10, player12));
        game9.setListPlayers(Arrays.asList(player2, player3, player4, player6, player8, player9, player10, player12));
        game10.setListPlayers(Arrays.asList(player2, player3, player4, player6, player8, player9, player10, player12));
        game11.setListPlayers(Arrays.asList(player2, player3, player4, player6, player8, player9, player10, player11, player12));
        game12.setListPlayers(Arrays.asList(player2, player3, player4, player6, player8, player9, player10, player11, player12));

        List<Games> gamesList = new ArrayList<>();
        gamesList.add(game1);
        gamesList.add(game2);
        gamesList.add(game3);
        gamesList.add(game4);
        gamesList.add(game5);
        gamesList.add(game6);
        gamesList.add(game7);
        gamesList.add(game8);
        gamesList.add(game9);
        gamesList.add(game10);
        gamesList.add(game11);
        gamesList.add(game12);


//        registration of players in the system (there should be a check,is nickname busy)
        PlayerComparator playerComparator = new PlayerComparator();
        Set<Player> playerList = new TreeSet<>(playerComparator);
        playerList.add(player1);
        if (playerList.contains(player2)) {
            System.err.println("Name " + player2.getName() + " used");
        } else {
            playerList.add(player2);
        }
        if (playerList.contains(player3)) {
            System.err.println("Name " + player3.getName() + " used");
        } else {
            playerList.add(player3);
        }
        if (playerList.contains(player4)) {
            System.err.println("Name " + player4.getName() + " used");
        } else {
            playerList.add(player4);
        }
        if (playerList.contains(player5)) {
            System.err.println("Name " + player2.getName() + " used");
        } else {
            playerList.add(player5);
        }
        if (playerList.contains(player6)) {
            System.err.println("Name " + player2.getName() + " used");
        } else {
            playerList.add(player6);
        }
        if (playerList.contains(player7)) {
            System.err.println("Name " + player2.getName() + " used");
        } else {
            playerList.add(player7);
        }
        if (playerList.contains(player8)) {
            System.err.println("Name " + player2.getName() + " used");
        } else {
            playerList.add(player8);
        }
        if (playerList.contains(player9)) {
            System.err.println("Name " + player2.getName() + " used");
        } else {
            playerList.add(player9);
        }
        if (playerList.contains(player10)) {
            System.err.println("Name " + player2.getName() + " used");
        } else {
            playerList.add(player10);
        }
        if (playerList.contains(player11)) {
            System.err.println("Name " + player2.getName() + " used");
        } else {
            playerList.add(player11);
        }
        if (playerList.contains(player12)) {
            System.err.println("Name " + player2.getName() + " used");
        } else {
            playerList.add(player12);
        }
        for (Player p:playerList){
            System.out.println(p.getName());
        }

        System.out.println();

//        adds a rating to the player, in case he wins the game


//        displays a list of games that all players on the site play

        playerList.stream()
                .flatMap(player -> player.getListGames().stream())
                .map(Games::getNameGame)
                .distinct()
                .forEach(System.out::println);

        System.out.println();

//        displays rating by player name and game
        playerList.stream()
                .flatMap(player -> player1.getListGames().stream())
                .filter(games -> games.equals(game1))
                .distinct()
                .forEach(System.out::println);


        System.out.println();

//         displays the top 10 players in a particular game
        gamesList.stream()
                .flatMap(games -> game1.getListPlayers().stream())
                .sorted((p1, p2) -> p2.getPlayerPoints() - p1.getPlayerPoints())
                .distinct()
                .forEach(System.out::println);

        System.out.println();

//        displays the top 10 players, taking into account all the games
        gamesList.stream()
                .flatMap(games -> games.getListPlayers().stream())
                .sorted((p1, p2) -> p2.getPlayerPoints() - p1.getPlayerPoints())
                .distinct()
                .forEach(System.out::println);
    }
}
