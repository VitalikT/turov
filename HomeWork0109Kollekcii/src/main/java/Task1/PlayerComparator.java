package Task1;

import java.util.Comparator;

public class PlayerComparator implements Comparator<Player> {
    public int compare(Player o1, Player o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
