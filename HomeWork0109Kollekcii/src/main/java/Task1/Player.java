package Task1;

import java.util.List;
import java.util.Random;

public class Player {

    Random random = new Random();

    private String name;
    private List<Games> listGames;
    private int playerPoints = random.nextInt(99);

    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getPlayerPoints() {
        return playerPoints;
    }

    public List<Games> getListGames() {
        return listGames;
    }

    public void setListGames(List<Games> listGames) {
        this.listGames = listGames;
    }


    @Override
    public String toString() {
        return name;
    }
}
