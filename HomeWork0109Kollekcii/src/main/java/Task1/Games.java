package Task1;

import java.util.List;
import java.util.Random;

public class Games {

     Random random = new Random();

    private String nameGame;
    private int points = random.nextInt(99);
    private List <Player> listPlayers;

    public String getNameGame() {
        return nameGame;
    }

    public int getPoints() {
        return points;
    }

    public void setListPlayers(List<Player> listPlayers) {
        this.listPlayers = listPlayers;
    }

    public List<Player> getListPlayers() {
        return listPlayers;
    }

    public int setPoints(int points) {
        this.points = points;
        return points;
    }

    public Games(String nameGame) {
        this.nameGame = nameGame;
    }

    @Override
    public String toString() {
        return "" + points;
    }
}
