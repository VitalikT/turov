package metody;
/*Exercise 1
Write and test methods of working with square matrices (matrixes should be presented in the form of two-dimensional
arrays).
Methods must be present:
■ create a single (diagonal) matrix;
■ create a zero matrix;
■ matrix addition;
■ matrix multiplication;
■ matrix multiplication by a scalar;
■ determination of the determinant of the matrix;
■ output matrix to the console.*/

import java.util.Arrays;

public class Task1 {
    private int[][] matrix = new int[5][5];
    private int[][] matrix1 = new int[5][5];
    private int[][] matrix2 = new int[5][5];

    public static void main(String[] args) {
        Task1 m = new Task1();
        m.diagonalMatrix();
        System.out.println();
        m.zeroMatrix();
        System.out.println();
        //m.sumMatrix();
    }

    void diagonalMatrix() {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (j > i) {
                    System.out.print((matrix.length - i + j) % matrix.length + " ");
                } else {
                    System.out.print((i - j + matrix.length) % matrix.length + " ");
                }
            }
            System.out.println();
        }

    }

    void zeroMatrix() {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (j == i) {
                    System.out.print(0 + " ");
                } else {
                    System.out.print(0 + " ");
                }
            }
            System.out.println();
        }

    }

}