package metody;
/*Task 2
Write and test the overloaded method that displays:
■ one-dimensional array of int type;
■ one-dimensional array of type String;
■ two-dimensional array of type int;
■ two-dimensional array of type float.*/

import java.util.ArrayList;
import java.util.Arrays;

public class Task2 {
    private int[] mas = new int[3];
    private String[] mas1 = new String[3];
    private int[][] mas2 = new int[][]{{0,1},{0,1}};
    private float[][] mas3 = new float[][]{{1f,1f},{1f,1f}};

    public static void main(String[] args) {
        Task2 pRint = new Task2();
        pRint.print();
        System.out.println();
        pRint.print(1);
        System.out.println();
        pRint.print("");
        System.out.println();
        pRint.print(1f);

    }

    public void print() {
        for (int i = 0; i < mas.length; i++) {
            System.out.print(i);
        }
    }

    public void print(String s) {
        ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList(mas1));
        System.out.print(arrayList);
    }

    public void print(int b) {
        for (int i = 0; i < mas2.length; i++) {
            for (int j = 0 ; j< mas2[i].length; j++) {
                System.out.print(mas2[i][j]+" ");
            }
            System.out.println();
        }
    }

    public void print(float f) {
        for (int i = 0; i < mas3.length; i++) {
            for (int j = 0 ; j< mas3.length; j++) {
                System.out.print(mas3[i][j]+" ");
            }
            System.out.println();
        }
    }


}