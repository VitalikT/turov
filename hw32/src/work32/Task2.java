package work32;
/*
Task 2
Write a program that displays all prime numbers in the range of 2 to 1,000,000. Try not to
perform unnecessary actions (for example, after you
found at least one nontrivial divider, it is already clear that
the number is composite and verification is not necessary to continue). Also
Note that the smallest divisor of a natural number n,
if it exists at all, it is necessarily located in the interval [2; √n].
 */

import static java.lang.System.out;

public class Task2 {
    public static void main(String[] args) {
        boolean flag = true;
        for (int i = 2; i <= 10000; i++) {
            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    flag = false;
                    break;
                }
            }
            if (flag) out.print(i + " ");
            flag = true;
        }


    }
}
