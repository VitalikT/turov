package work32;
/*
Task 5
Write a program that will check is
whether the number entered from the keyboard by the palindrome (equally
read in both directions). For example, 123454321
or 221122 - palindrome. The program should display YES,
if the number is a palindrome, and NO is otherwise.
*/

import static java.lang.System.out;

import java.util.Scanner;

public class Task5 {

    public static void main(String[] args) {
        out.print("Enter number: ");
        Scanner scan = new Scanner(System.in);
        String number = scan.next();
        isPalindrome(number);
    }

    public static String reverseString(String s) {
        String r = "";
        for (int i = s.length() - 1; i >= 0; --i)
            r += s.charAt(i);
        return r;


    }

    public static Boolean isPalindrome(String s) {
        if (s.equals(reverseString(s))) {
            out.println("Palindrome");
        } else {
            out.println("No palindrome");
        }
        return s.equals(reverseString(s));

    }
}

//decided not himself
