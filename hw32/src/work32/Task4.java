package work32;
/*
Task 4
Display all perfect numbers in the range of
0 to 1,000,000.
Examples:
1st perfect number: 6 has the following own
dividers - 1, 2, 3; their sum is 6.
2nd perfect number: 28 has the following own
dividers: - 1, 2, 4, 7, 14; their sum is 28.
 */


import static java.lang.System.out;

public class Task4 {
    public static void main(String[] args) {
        for (int x = 1; x <= 1000000; x++) {
            int sum = 0;
            for (int j = 1; j < x; j++) {
                if (x % j == 0) {
                    sum = sum + j;
                }
            }
            if (sum==x) {
                out.println(sum);
            }

        }
    }
}


