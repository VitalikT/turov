package work32;
/*
Task 11
Write a program that displays numbers
from 1 to 1 000. Instead of numbers that are multiples of three,
the program should output the word fizz, and instead of numbers that are multiples of
five - the word buzz. If the number is a multiple of fifteen,
then the program should output the word hiss instead of the number
 */

import static java.lang.System.out;

public class Task11 {
    public static void main(String[] args) {
        int j = 0;
        while (j < 1000) {
            j++;
            if (j % 15 == 0&j % 5 == 0&j % 3 == 0) {
                out.print("hiss ");
                continue;
            }
            if (j % 3 == 0) {
                out.print("fizz ");
                continue;
            }
            if (j % 5 == 0) {
                out.print("buzz ");
                continue;
            }
            out.print(j+" ");
        }
    }
}
