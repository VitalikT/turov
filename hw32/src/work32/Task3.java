package work32;
/*
Task 3
Display all Armstrong numbers ranging from
10 to 1,000,000.
For example:
153 = 1^3 + 5^3 + 3^3
 */
import static java.lang.Math.pow;
import static java.lang.System.out;

public class Task3 {
    public static void main(String[] args) {
        for (int n = 10; n < 1000000; n++) {
            int r5, r4, r3, r2, r1, r0;
            r0 = (n % 10);
            r1 = (((n - r0) / 10) % 10);
            r2 = (((n - (r0 + (10 * r1))) / 100) % 10);
            r3 = (((n - (r0 + (10 * r1) + (100 * r2))) / 1000) % 10);
            r4 = (((n - (r0 + (10 * r1) + (100 * r2) + (1000 * r3))) / 10000) % 10);
            r5 = (((n - (r0 + (10 * r1) + (100 * r2) + (1000 * r3) + (10000 * r4))) / 100000) % 10);
            int x2 = 2;
            int x3 = 3;
            int x4 = 4;
            int x5 = 5;
            int x6 = 6;
            if ((pow(r1, x2) + pow(r0, x2)) == n)
                out.println(n);
            if ((pow(r2, x3) + pow(r1, x3) + pow(r0, x3)) == n)
                out.println(n);
            if ((pow(r3, x4) + pow(r2, x4) + pow(r1, x4) + pow(r0, x4)) == n)
                out.println(n);
            if ((pow(r4, x5) + pow(r3, x5) + pow(r2, x5) + pow(r1, x5) + pow(r0, x5)) == n)
                out.println(n);
            if ((pow(r5, x6) + pow(r4, x6) + pow(r3, x6) + pow(r2, x6) + pow(r1, x6) + pow(r0, x6)) == n)
                out.println(n);
        }
    }
}

