package inkapsulacia;
/*Exercise 1
Create a program that allows you to enter n ranges of integer values ​​from the keyboard (for example,
3-10). Each range has a start and end point.
After entering the program should display the length of each range. The program should display an error message
if the beginning of the range is greater than the end. Test with
using JUnit.
Example:
Input: 3 9, 2 4, 11 15
Weekend 3, 6, 4
*/

import java.util.Scanner;

public class Task1 {

    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        Range man = new Range();
        man.renge3();

    }

    static class Range {

        void renge3() {
            System.out.print("Input: ");
            String text = sc.nextLine();
            System.out.print("Weekend:");
            String[] parts = text.split(", ", text.length());
            for (int i = 0; i < parts.length; i++) {
                String[] s = parts[i].split(" ");
                int startingPoint = Integer.parseInt(s[0]);
                int endPoint = Integer.parseInt(s[1]);
                if (startingPoint > endPoint) {
                    System.out.print("Mistake. The beginning of the range is greater than the end.");
                } else {
                    int sum = 0;
                    for (int p = startingPoint; p <= endPoint; p++) {
                        sum += 1;
                    }
                    System.out.print(sum + " ");
                }
            }
        }


    }
}
