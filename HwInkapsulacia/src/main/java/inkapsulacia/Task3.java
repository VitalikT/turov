package inkapsulacia;

public class Task3 {


    public static void main(String[] args) {
        Money belarus = new Money();
        belarus.setRubles(1);
        belarus.setPennies((byte) 50);
        Money belarus2 = new Money();
        belarus2.setRubles(2);
        belarus2.setPennies((byte) 50);
        belarus.addition(belarus2);
//        belarus.subtraction(belarus2);
//        belarus.operationsComparisons(belarus2);
//        belarus.multiplicationByFractionalNumber(belarus2);
//        belarus.divisionOfSumByFractionalNumber(belarus2);
        belarus.print();
    }

    static class Money {
        private long rubles;
        private byte pennies;

        public long getRubles() {
            return rubles;
        }

        public byte getPennies() {
            return pennies;
        }

        public void setRubles(long rubles) {
            this.rubles = rubles;
        }

        public void setPennies(byte pennies) {
            this.pennies = pennies;
        }

        void print() {
            System.out.println(rubles + "," + pennies);
        }

        public void addition(Money g) {
            long newRubles = this.rubles + g.getRubles();
            byte newPennies = (byte) (this.pennies + g.getPennies());
            if (this.pennies >= 50 & g.getPennies() >= 50) {
                long newPennies2 = ((long) (this.pennies + g.getPennies())) % 100;
                newPennies = (byte) newPennies2;
                newRubles += 1;
            }
            this.rubles = newRubles;
            this.pennies = newPennies;
        }

        void subtraction(Money g) {

            long newRubles = this.rubles - g.getRubles();
            byte newPennies = (byte) (this.pennies - g.getPennies());
            if (this.rubles < g.getRubles()) {
                System.out.println("Insufficient funds");
            } else if (this.pennies < g.getPennies()) {
                long newPennies2 = ((long) ((this.pennies + 100) - g.getPennies())) % 100;
                newPennies = (byte) newPennies2;
                newRubles -= 1;
            }
            this.rubles = newRubles;
            this.pennies = newPennies;
        }

        void divisionOfSumByFractionalNumber(Money g) {
            double newFractionlNamber = (this.rubles * 100 + this.pennies) * 100 / (g.getRubles() * 100 + g.getPennies());
            double newRubles2 = (newFractionlNamber - newFractionlNamber % 100) / 100;
            long newRubles = (long) newRubles2;
            double newPennies2 = newFractionlNamber % 100;
            byte newPennies = (byte) newPennies2;
            this.rubles = newRubles;
            this.pennies = newPennies;
        }

        void multiplicationByFractionalNumber(Money g) {
            long newFractionlNamber = ((this.rubles * 100 + this.pennies) * (g.getRubles() * 100 + g.getPennies())) / 100;
            long newRubles = (newFractionlNamber - newFractionlNamber % 100) / 100;
            long newPennies2 = newFractionlNamber % 100;
            byte newPennies = (byte) newPennies2;
            this.rubles = newRubles;
            this.pennies = newPennies;

        }

        void operationsComparisons(Money g) {
            if (this.rubles >= g.getRubles()) {
                long newRubles = this.rubles - g.getRubles();
                byte newPennies = (byte) (this.pennies - g.getPennies());
                if (this.pennies < g.getPennies()) {
                    long newPennies2 = ((long) ((this.pennies + 100) - g.getPennies())) % 100;
                    newPennies = (byte) newPennies2;
                    newRubles -= 1;
                }
                this.rubles = newRubles;
                this.pennies = newPennies;
                System.out.println("more on ");
            } else {
                long newRubles = g.getRubles() - this.rubles;
                byte newPennies = (byte) (g.getPennies() - this.pennies);
                if (this.pennies > g.getPennies()) {
                    long newPennies2 = ((long) ((g.getPennies() + 100) - this.pennies)) % 100;
                    newPennies = (byte) newPennies2;
                    newRubles -= 1;
                }
                this.rubles = newRubles;
                this.pennies = newPennies;
                System.out.println("less on ");
            }
        }

        @Override
        public String toString() {
            return rubles + "," + pennies;
        }

    }
}