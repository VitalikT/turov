package hw42;
/*
Task 2
Right triangle,
right up and right angle:
filled and empty.
 */

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter length: ");
        int length = sc.nextInt();
        for (int i = 0; i <= length - 1; i++) {
            for (int j = length; j <= i - 1; j++) {
                System.out.print(" ");
            }
            for (int b = 0; b <= length - 1; b++) {
                if ( i == 0 | i == b | b == length - 1 ) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
