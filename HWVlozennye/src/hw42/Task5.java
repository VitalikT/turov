package hw42;
/*
Task 5
Parallelogram: filled and empty.
 */

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter length: ");
        int length = sc.nextInt();
        for (int i = 0; i <= length - 1; i++) {
            for (int j = 0; j <= i - 1; j++) {
                System.out.print(" ");
            }
            for (int b = 0; b <= length - 1; b++) {
                if ( i == 0 | i == length - 1 | b == length - 1 | b == 0 ) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
