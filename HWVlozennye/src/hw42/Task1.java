package hw42;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter length: ");
        int length = sc.nextInt();
        for (int i = 0; i <= length - 1; i++) {
            for (int j = length - 1; j > i; j--) {
                System.out.print(" ");
            }
            for (int b = 0; b <= i; b++) {
                if (b == 0 | b == length - 1 | i == b | i == length - 1 ) {
                    System.out.print("*");
                } else System.out.print(" ");
            }
            System.out.println();
        }
    }
}
